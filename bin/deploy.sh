#!/bin/bash

SRCDIR="${HOME}/lisperes/"
DSTDIR="${HOME}/common-lisp/lisperes/"

HTDOCSDIR="${SRCDIR}/htdocs/"
PUBLICDIR="${HOME}/public_html/"

syncdir () {
    dir=$1
    targetdir="${PUBLICDIR}/$dir/"
    rsync -avzt --delete "${HTDOCSDIR}/$dir/" "${targetdir}"
    find $targetdir -type f -exec chmod 644 {} \;
    find $targetdir -type d -exec chmod 755 {} \;
}

deploy () {
    rsync -avzt --delete --exclude .git $SRCDIR $DSTDIR
    syncdir css
    syncdir fonts
    syncdir img
}

while : ; do
        echo "Waiting for changes in $SRCDIR..."
        inotifywait -q ${SRCDIR}
        echo "$(date): Changes detected in $SRCDIR. Deploying..."
        deploy
        sleep 2
done
