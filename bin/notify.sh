#!/bin/bash

MAILADDRESS=me@me.com

LOGFILE="${HOME}/log/lisperes/cgi.log"

a=$(sha1sum $LOGFILE)
while : ; do
        b=$(sha1sum $LOGFILE)
        if [ "$a" != "$b" ] ; then
                echo "$(date) Notifying... a = $a . b = $b"
                echo "Number: $(wc -l $LOGFILE)" | mail -s "Lisper.es" $MAILADDRESS 
                a=$b
        fi
        sleep 3600
done
