#!/usr/bin/perl

# Proxy to publish the app

#use LWP::UserAgent;
use warnings;

use DateTime qw( );
use File::Spec;
my ($vol, $dir, $file) = File::Spec->splitpath($ENV{SCRIPT_FILENAME});
my $logfile = "$dir/../log/lisperes/cgi.log";

sub printenvvars {
    foreach $key (sort keys(%ENV)) {
        print FH "$key = $ENV{$key}\n";
    }
}

sub logit {
    # TODO: Use interpolation
    open(FH, '>>', $logfile) or die "Could not open file '$logfile' $!";
    print FH $ENV{REMOTE_ADDR};
    print FH " - ";
    my $dt = DateTime->now( time_zone => 'local' );
    $dt->subtract( days => 7 );
    print FH "[";
    print FH $dt->strftime("%Y-%m-%d %H:%M:%S");
    print FH "] \"";
    print FH $ENV{REQUEST_METHOD};
    print FH " ";
    print FH $ENV{REQUEST_URI};
    print FH " ";
    print FH $ENV{SERVER_PROTOCOL};
    print FH "\" ";
    ## ....
    ##127.0.0.1 - [2022-08-28 16:00:04] "GET / HTTP/1.1" 200 7445 "-" "curl/7.68.0"
    print FH $ENV{HTTP_USER_AGENT};
    print FH "\n";
    close FH;
}

#my $uri = $ENV{REQUEST_URI};
#$uri = s/~nimiux//;

# URL
#my $url = URI->new( 'http://127.0.0.1:4242' );

# user agent to use a proxy
#my $user_agent = LWP::UserAgent->new;
#$user_agent->proxy( 'http', 'http://our_proxy:port/' );

# request
#my $req = HTTP::Request->new( GET => "$url/$uri" );

# response
#my $res = $user_agent->request( $req );

# Log it
logit;

# Render content
print "Content-Type: text/html\n\n";
system "curl http://127.0.0.1:4242";
#print $res->content;
